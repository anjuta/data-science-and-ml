import numpy as np
import pandas as pd

df = pd.read_csv('/Users/anjuta/Downloads/altoran_personal_likings.csv')
df.shape

likes_mean = df.groupby(['user_pseudo_id', 'publisher_name'])['likes'].mean()
likes_mean_sort = likes_mean.sort_values(ascending=False)

likes_count = df.groupby(['user_pseudo_id', 'publisher_name'])['likes'].count()
likes_count_sort = likes_count.sort_values(ascending=False)

df_mean_count = pd.DataFrame(likes_mean)
df_mean_count['likes_count'] = pd.DataFrame(likes_count)
top_likes = df_mean_count.sort_values('likes_count', ascending=False)
most_likes_product = top_likes.index.values[0][1]

user_product_likes = df.pivot_table(index='user_pseudo_id', columns='publisher_name', values='likes')
user_product_likes.fillna(0, inplace=True)

similar_to_most_likes_product = user_product_likes[most_likes_product]
corr_products = user_product_likes.corrwith(similar_to_most_likes_product)

corr_df = pd.DataFrame(corr_products, columns=['Correlation'])
corr_df.dropna(inplace=True)

corr_df.sort_values('Correlation', ascending=False)
corr_df = corr_df.join(df_mean_count['likes_count'])

corr_df_sort = corr_df[corr_df['likes_count']>=5].sort_values('Correlation', ascending=False)

print(corr_df_sort)